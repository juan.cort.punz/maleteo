<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class OpinionFormType extends AbstractType{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array(
            'label' => 'Nombre',
            'attr' => ['placeholder' => 'ej: Vicent Chase']
            
        ))
        ->add('city', TextType::class,array(
            'label' => 'Ciudad',
            'attr' => ['placeholder' => 'ej: Madrid']
        ))
        ->add('message', TextareaType::class,array(
            'label' => 'Opinión',
            'attr' => ['placeholder' => 'Escribe una opinión..']
        ))
        ->add('enviar', SubmitType::class);                    
    }



}