<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class DemoFormType extends AbstractType{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array(
            'label' => 'Nombre',
            'attr' => ['placeholder' => 'Vicent Chase']
            
        ))
        ->add('email', EmailType::class, array(
            'label' => 'Email',
            'attr' => ['placeholder' => 'vicent@mga.com']))
        ->add('ciudad',
        ChoiceType::class,[
            'choices' =>[
                'Madrid' => 'madrid',
                'Almería' => 'almeria',
                'Málaga' => 'malaga',
                'Córdoba' => 'cordoba'
                        ]
                         ]
                            )
        ->add('policy', CheckboxType::class,
        ['label' =>  "Aceptas la <a href='#'>política de privacidad </a>",
         'label_html' => true])
        ->add('enviar', SubmitType::class);                    
    }



}