<?php


namespace App\Controller;

use App\Form\DemoFormType;
use App\Form\OpinionFormType;
use App\Entity\OpinionRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class HomeController extends AbstractController
{


/**
 * @Route("/maleteo", name="homepage")
 */

public function showHome(EntityManagerInterface $doctrine){
    $form = $this->createForm(DemoFormType::class);
    $opForm = $this->createForm(OpinionFormType::class);
    $rep = $doctrine->getRepository(OpinionRequest::class);
    $newOpinion = $rep->findAll();
    shuffle($newOpinion);
   

    return $this->render('home.html.twig', ['demoForm' => $form->createView(), 'opinionForm' => $opForm->createView(), 'newOpinion' => $newOpinion]);
}



 }

