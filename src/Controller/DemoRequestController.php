<?php

namespace App\Controller;

use App\Entity\DemoRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\DemoFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class DemoRequestController extends AbstractController{
/**
 * @Route("/sendForm", name="checkForm")
 */

 public function insertRequest(EntityManagerInterface $doctrine, Request $request){
     
     $form = $this->createForm(DemoFormType::class);

    $form->handleRequest(($request));

    if($form->isSubmitted() && $form->isValid()){
        $date = $form->getData();
        $newDemo = new DemoRequest();
        $newDemo->setName($date['name']);
        $newDemo->setMail($date['email']);
        $newDemo->setCity($date['ciudad']);
   
        $doctrine->persist($newDemo);
        $doctrine->flush($newDemo);


        $this->addFlash('success', 'La solicitud se ha registrado correctamente.');

        return $this->redirectToRoute('homepage');

        
    }else{
        return $this->render('home.html.twig', ['demoForm' => $form->createView()]);

    }
 }

 /**
  * @Route("/listDemo")
  */

 public function getDemoRequest(EntityManagerInterface $doctrine){

    $rep = $doctrine->getRepository(DemoRequest::class);
    $newDemo = $rep->findAll();

    return $this->render('listDemo.html.twig', ['newDemo' => $newDemo]);
 }

 /**
  * @Route("/editDemo/{id}")
  */

  /* public function editDemoRequest($id){

    $entityManager = $this->getDoctrine()->getManager();
    $product = $entityManager->getRepository(DemoRequest::class)->find($id);

  } */

  /**
   * @Route("/deleteDemo/{id}")
   */

   public function removeDemoRequest($id){
    $entityManager = $this->getDoctrine()->getManager();
    $demo = $entityManager->getRepository(DemoRequest::class)->find($id);
    $entityManager->remove($demo);
    $entityManager->flush();
    $this->addFlash('success', 'Solicitud de demo eliminada.');

    return $this->redirectToRoute('homepage');
   }

}