<?php

namespace App\Controller;

use App\Entity\OpinionRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\OpinionFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class OpinionRequestController extends AbstractController{
/**
 * @Route("/sendOpinion", name="sendOpinion")
 */

 public function insertRequestOpinion(EntityManagerInterface $doctrine, Request $request){
     
     $form = $this->createForm(OpinionFormType::class);

    $form->handleRequest(($request));

    if($form->isSubmitted() && $form->isValid()){
        $date = $form->getData();
        $newOpinion = new OpinionRequest();
        $newOpinion->setName($date['name']);
        $newOpinion->setMessage($date['message']);
        $newOpinion->setCity($date['city']);
   
        $doctrine->persist($newOpinion);
        $doctrine->flush($newOpinion);


        $this->addFlash('success', 'La opinión ha sido registrada.');

        return $this->redirectToRoute('homepage');

        
    }else{
        return $this->render('home.html.twig', ['opinionForm' => $form->createView()]);

    }
 }

 

 public function getDemoRequest(EntityManagerInterface $doctrine){

    $rep = $doctrine->getRepository(OpinionRequest::class);
    $newOpinion = $rep->findAll();

    return $this->render('home.html.twig', ['newOpinion' => $newOpinion]);
 }

}