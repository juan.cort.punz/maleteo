<?php

namespace App\Repository;

use App\Entity\DemoRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DemoRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method DemoRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method DemoRequest[]    findAll()
 * @method DemoRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DemoRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DemoRequest::class);
    }

    // /**
    //  * @return DemoRequest[] Returns an array of DemoRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DemoRequest
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
