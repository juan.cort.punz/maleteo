<?php

namespace App\Repository;

use App\Entity\OpinionRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OpinionRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method OpinionRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method OpinionRequest[]    findAll()
 * @method OpinionRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OpinionRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OpinionRequest::class);
    }

    // /**
    //  * @return OpinionRequest[] Returns an array of OpinionRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OpinionRequest
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
